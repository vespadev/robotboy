import requests, json

class Requests:

    url = None
    last_resp = None
    def __init__(self,hwid):
        self.url = "http://192.168.100.5/api/"
        response = requests.get(str(self.url)+"get-setup/"+str(hwid))
        print(response.status_code)

    def get_command(self,id):
        response = requests.get("http://192.168.100.5/api/get-commands/"+str(id))
        return response.json()

    def post_new_setup(self, payload):

        header = {"Content-type": "application/x-www-form-urlencoded",
          "Accept": "text/plain"}

        path = str(self.url) + "post-setup"
        response = requests.post(path, data=payload, headers=header)
        self.last_resp = response.json()

    def update_cmd_status(self, cmd_id):

        header = {"Content-type": "application/x-www-form-urlencoded",
          "Accept": "text/plain"}

        path = str(self.url) + "update-cmd-status/"+ str(cmd_id)
        response = requests.get(path)
        print(response.status_code)
